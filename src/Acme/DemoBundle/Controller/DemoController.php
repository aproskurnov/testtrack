<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Acme\DemoBundle\Form\ContactType;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;


class DemoController extends Controller
{
    /**
     * @Route("/", name="_demo")
     * @Template()
     */
    public function indexAction()
    {

        $e = new MessageDigestPasswordEncoder('sha512', true, 10);

        $salt1 = md5('111');
        $salt2 = md5('222');
        $salt3 = md5('333');

        $pass1 = $e->encodePassword('qwerty1', $salt1);
        $pass2 = $e->encodePassword('qwerty2', $salt2);
        $pass3 = $e->encodePassword('qwerty3', $salt3);
        var_dump(array('pass'=>$pass1, 'salt'=>$salt1));
        var_dump(array('pass'=>$pass2, 'salt'=>$salt2));
        var_dump(array('pass'=>$pass3, 'salt'=>$salt3));

        return array();
    }

    /**
     * @Route("/hello/{name}", name="_demo_hello")
     * @Template()
     */
    public function helloAction($name)
    {
        return array('name' => $name);
    }

    /**
     * @Route("/contact", name="_demo_contact")
     * @Template()
     */
    public function contactAction()
    {
        $form = $this->get('form.factory')->create(new ContactType());

        $request = $this->get('request');
        if ($request->isMethod('POST')) {
            $form->submit($request);
            if ($form->isValid()) {
                $mailer = $this->get('mailer');
                // .. setup a message and send it
                // http://symfony.com/doc/current/cookbook/email.html

                $this->get('session')->getFlashBag()->set('notice', 'Message sent!');

                return new RedirectResponse($this->generateUrl('_demo'));
            }
        }

        return array('form' => $form->createView());
    }
}
