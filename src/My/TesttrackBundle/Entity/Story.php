<?php

namespace My\TesttrackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use My\TesttrackBundle\DBAL\StateStoryType;

/**
 * Story
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Story
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false )
     */
    private $user;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="state",type="string", type="enumstatestory", nullable=false)

     *
     */
    private $state;

//* type="string", columnDefinition="ENUM('new', 'started', 'finished', 'accepted', 'rejected')")
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \int $user
     * @return Story
     */
    public function setUserId($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \int 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \string $state
     * @return \int
     */
    public function setUser( $user )
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set state
     *
     * @param \string $state
     * @return Story
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return \string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set title
     *
     * @param \string $title
     * @return Story
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return \string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Story
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @inheritDoc
     */
    function __toString()
    {
        return $this->getTitle();
    }

}
