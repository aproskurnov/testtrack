<?php

namespace My\TesttrackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Storycomment
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Storycomment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Story")
     * @ORM\JoinColumn(name="story_id", referencedColumnName="id", nullable=false)
     */
    private $story;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string")
     */
    private $text;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Storycomment
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set user
     *
     * @param \My\TesttrackBundle\Entity\User $user
     * @return Storycomment
     */
    public function setUser(\My\TesttrackBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \My\TesttrackBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set story
     *
     * @param \My\TesttrackBundle\Entity\Story $story
     * @return Storycomment
     */
    public function setStory(\My\TesttrackBundle\Entity\Story $story = null)
    {
        $this->story = $story;
    
        return $this;
    }

    /**
     * Get story
     *
     * @return \My\TesttrackBundle\Entity\Story 
     */
    public function getStory()
    {
        return $this->story;
    }
}