/**
 * Created by lexxa on 10.11.13.
 */

$(function(){
    $("#users, #states").change( function(){
        getStory();
    })

    function getStory(){
        var url = $("#entities").attr("url");
        var filterUsr = $("#users option:selected").attr("data-id");
        var filterState = $("#states option:selected").val();

        $.ajax({
            method: "POST",
            url: url,
            data:{
                users:filterUsr,
                states: filterState
            }
        }).done(function( resp ) {
                $("#entities").html( resp );
            });
    }
})