<?php
/**
 * Created by PhpStorm.
 * User: lexxa
 * Date: 10.11.13
 * Time: 20:16
 */

namespace My\TesttrackBundle\DBAL;

class StateStoryType extends EnumType
{
    protected $name = 'enumstatestory';
    static protected $values = array('new', 'started', 'finished', 'accepted', 'rejected');
}