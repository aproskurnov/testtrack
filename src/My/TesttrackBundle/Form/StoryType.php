<?php

namespace My\TesttrackBundle\Form;

use My\TesttrackBundle\DBAL\StateStoryType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StoryType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        foreach (StateStoryType::getValues() as $state){
            $choices[$state] = $state;
        }

        $builder
            ->add('state', 'choice', array(
                'choices'   => $choices,
                'label'=>'Состояние:'
                )

            )
            ->add('title','text', array('label'=>'Заголовок:'))
            ->add('description','textarea', array('label'=>'Описание:'))
            ->add('user', null, array('label'=>'Ответственный'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'My\TesttrackBundle\Entity\Story'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'my_testtrackbundle_story';
    }
}
