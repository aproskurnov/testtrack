<?php

namespace My\TesttrackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StorycommentType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', 'textarea', array('label'=>'текст'))
            ->add('user', null, array('label'=>'от пользователя'))
            ->add('story', null, array('label'=>'к задаче'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'My\TesttrackBundle\Entity\Storycomment'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'my_testtrackbundle_storycomment';
    }
}
