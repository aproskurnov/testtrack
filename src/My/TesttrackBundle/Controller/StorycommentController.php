<?php

namespace My\TesttrackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use My\TesttrackBundle\Entity\Storycomment;
use My\TesttrackBundle\Form\StorycommentType;

/**
 * Storycomment controller.
 *
 * @Route("/{storyid}/storycomment")
 */
class StorycommentController extends Controller
{

    /**
     * Lists all Storycomment entities.
     *
     * @Route("/", name="storycomment")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TesttrackBundle:Storycomment')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Storycomment entity.
     *
     * @Route("/", name="storycomment_create")
     * @Method("POST")
     * @Template("TesttrackBundle:Storycomment:new.html.twig")
     */
    public function createAction(Request $request, $storyid)
    {


        $entity = new Storycomment();
        $form = $this->createCreateForm($entity, $storyid);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('story_show', array('id' => $storyid)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Storycomment entity.
    *
    * @param Storycomment $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Storycomment $entity, $storyid)
    {
        $form = $this->createForm(new StorycommentType(), $entity, array(
            'action' => $this->generateUrl('storycomment_create', array('storyid'=>$storyid,'id'=>$entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Создать'));

        return $form;
    }

    /**
     * Displays a form to create a new Storycomment entity.
     *
     * @Route("/new", name="storycomment_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction( $storyid )
    {
        $entity = new Storycomment();
        $form   = $this->createCreateForm($entity, $storyid);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'storyid' => $storyid
        );
    }

    /**
     * Finds and displays a Storycomment entity.
     *
     * @Route("/{id}", name="storycomment_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TesttrackBundle:Storycomment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Storycomment entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Storycomment entity.
     *
     * @Route("/{id}/edit", name="storycomment_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $storyid)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TesttrackBundle:Storycomment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Storycomment entity.');
        }

        $editForm = $this->createEditForm($entity,$storyid);
        $deleteForm = $this->createDeleteForm($id, $storyid);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'storyid'     => $storyid
        );
    }

    /**
    * Creates a form to edit a Storycomment entity.
    *
    * @param Storycomment $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Storycomment $entity, $storyid)
    {
        $form = $this->createForm(new StorycommentType(), $entity, array(
            'action' => $this->generateUrl('storycomment_update', array('storyid'=>$storyid, 'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Сохранить'));

        return $form;
    }
    /**
     * Edits an existing Storycomment entity.
     *
     * @Route("/{id}", name="storycomment_update")
     * @Method("PUT")
     * @Template("TesttrackBundle:Storycomment:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $storyid)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TesttrackBundle:Storycomment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Storycomment entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $storyid);
        $editForm = $this->createEditForm($entity, $storyid);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('story_show', array('id' => $storyid)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Storycomment entity.
     *
     * @Route("/{id}", name="storycomment_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $storyid)
    {
        $form = $this->createDeleteForm($id, $storyid);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TesttrackBundle:Storycomment')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Storycomment entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('story_show', array('id' => $storyid)));
    }

    /**
     * Creates a form to delete a Storycomment entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $storyid)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('storycomment_delete', array('storyid'=>$storyid, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Удалить'))
            ->getForm()
        ;
    }
}
