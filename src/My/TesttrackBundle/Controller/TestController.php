<?php
/**
 * Created by PhpStorm.
 * User: lexxa
 * Date: 09.11.13
 * Time: 21:59
 */

namespace My\TesttrackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller
{
    public function indexAction()
    {

        $user = $this->getUser();
        return $this->render('TesttrackBundle:Default:index.html.twig', array('name' => $user->getUsername()));
    }
}